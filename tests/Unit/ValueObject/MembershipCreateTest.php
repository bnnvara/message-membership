<?php

declare(strict_types=1);

namespace Tests\BNNVARA\Membership\Unit\ValueObject;

use BNNVARA\Membership\ValueObject\MembershipCreate;
use PHPUnit\Framework\TestCase;

class MembershipCreateTest extends TestCase
{
    /**
     * @test
     * @dataProvider validMembershipCreateProvider
     */
    public function aValidMembershipCreateCanBeCreated(
        string $membershipId,
        string $initials,
        string $firstName,
        ?string $nameAffix,
        string $lastName,
        string $gender,
        string $dateOfBirth,
        string $postalCode,
        int $houseNumber,
        ?string $houseNumberAddition,
        string $street,
        string $city,
        string $emailAddress,
        string $phoneNumber,
        string $actionCode,
        ?string $premiumCode,
        int $membershipCost
    ): void {
        $membershipCreate = new MembershipCreate(
            membershipId: $membershipId,
            initials: $initials,
            firstName: $firstName,
            lastName: $lastName,
            gender: $gender,
            dateOfBirth: $dateOfBirth,
            postalCode: $postalCode,
            houseNumber: $houseNumber,
            street: $street,
            city: $city,
            emailAddress: $emailAddress,
            phoneNumber: $phoneNumber,
            actionCode: $actionCode,
            membershipCost: $membershipCost,
            nameAffix: $nameAffix,
            houseNumberAddition: $houseNumberAddition,
            premiumCode: $premiumCode
        );

        $this->assertSame($membershipId, $membershipCreate->getMembershipId());
        $this->assertSame($initials, $membershipCreate->getInitials());
        $this->assertSame($firstName, $membershipCreate->getFirstName());
        $this->assertSame($nameAffix, $membershipCreate->getNameAffix());
        $this->assertSame($lastName, $membershipCreate->getLastName());
        $this->assertSame($gender, $membershipCreate->getGender());
        $this->assertSame($dateOfBirth, $membershipCreate->getDateOfBirth());
        $this->assertSame($postalCode, $membershipCreate->getPostalCode());
        $this->assertSame($houseNumber, $membershipCreate->getHouseNumber());
        $this->assertSame($houseNumberAddition, $membershipCreate->getHouseNumberAddition());
        $this->assertSame($street, $membershipCreate->getStreet());
        $this->assertSame($city, $membershipCreate->getCity());
        $this->assertSame($emailAddress, $membershipCreate->getEmailAddress());
        $this->assertSame($phoneNumber, $membershipCreate->getPhoneNumber());
        $this->assertSame($actionCode, $membershipCreate->getActionCode());
        $this->assertSame($premiumCode, $membershipCreate->getPremiumCode());
        $this->assertSame($membershipCost, $membershipCreate->getMembershipCost());
    }

    public function validMembershipCreateProvider(): array
    {
        return [
            [
                '91067c81-f45f-450b-8e28-fdbb47f28906',
                'initials',
                'firstName',
                'nameAffix',
                'lastName',
                'male',
                '2000-01-15',
                '1217 ZR',
                123,
                'houseNumberAddition',
                'street',
                'city',
                'zz@yy.nl',
                '+31656892315',
                '9024e611-8654-4b3f-8838-c918ad138dfc',
                '7673782b-6850-427c-a212-5f4774296640',
                200,
            ],
            [
                'e63a101a-9d04-4ed8-a64a-4bcabd88c0d3',
                'AA',
                'BB',
                null,
                'CC',
                'female',
                '1990-01-02',
                '1217ZR',
                987,
                null,
                'DDDD',
                'EEEE',
                'xxx@www.nl',
                '+31612457896',
                '4bb7a1d0-a0fd-4034-8301-cde4a0e3abed',
                null,
                9999,
            ],
        ];
    }
}
