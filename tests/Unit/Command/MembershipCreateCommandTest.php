<?php

declare(strict_types=1);

namespace Tests\BNNVARA\Membership\Unit\Command;

use BNNVARA\Membership\Command\MembershipCreateCommand;
use BNNVARA\Membership\ValueObject\MembershipCreate;
use PHPUnit\Framework\TestCase;

class MembershipCreateCommandTest extends TestCase
{
    /** @test */
    public function aMembershipCreateCommandCanBeCreated(): void
    {
        $membershipCreate = $this->getMembershipCreateMock();

        $command = new MembershipCreateCommand($membershipCreate);

        $this->assertInstanceOf(MembershipCreateCommand::class, $command);
        $this->assertInstanceOf(MembershipCreate::class, $command->getData());
        $this->assertSame($command->getData(), $membershipCreate);
    }

    private function getMembershipCreateMock(): MembershipCreate
    {
        $membershipCreate = $this
            ->getMockBuilder(MembershipCreate::class)
            ->disableOriginalConstructor()
            ->getMock();

        /** @var MembershipCreate $membershipCreate */
        return $membershipCreate;
    }
}
