<?php

declare(strict_types=1);

namespace Tests\BNNVARA\Membership\Unit\Infrastructure\Messenger\RabbitMq;

use BNNVARA\Membership\Command\MembershipCreateCommand;
use BNNVARA\Membership\Infrastructure\Messenger\RabbitMq\RoutingKeys;
use PHPUnit\Framework\TestCase;

class RoutingKeysTest extends TestCase
{
    /** @test */
    public function getRoutingKeysTest(): void
    {
        $routingKeys = new RoutingKeys();
        $keys = $routingKeys->getRoutingKeys();
        $this->assertCount(1, $keys);
        $this->assertSame('membership.command.create', $keys[MembershipCreateCommand::class]);
    }
}
