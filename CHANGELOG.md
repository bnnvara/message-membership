# Changelog
All notable changes to this project will be documented in this file. Note: Changelogs are for HUMANS, not computers.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres
to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.3] - 2021-02-10
### Added
- .editorconfig

### Removed
- Value objects

## [0.2] - 2021-02-09
### Changed
- Switch to PHP 7.4 as minimum requirement

## [0.1] - 2021-02-02
### Added
- Docker
- PHPUnit
- MembershipCreateCommand
