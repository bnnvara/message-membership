<?php

declare(strict_types=1);

namespace BNNVARA\Membership\Command;

use BNNVARA\Membership\ValueObject\MembershipCreate;

class MembershipCreateCommand
{
    private MembershipCreate $data;

    public function __construct(MembershipCreate $data)
    {
        $this->data = $data;
    }

    public function getData(): MembershipCreate
    {
        return $this->data;
    }
}
