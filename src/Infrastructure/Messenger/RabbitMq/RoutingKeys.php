<?php

declare(strict_types=1);

namespace BNNVARA\Membership\Infrastructure\Messenger\RabbitMq;

use BNNVARA\Membership\Command\MembershipCreateCommand;
use BNNVARA\MessengerBundle\RabbitMQ\RoutingKey\RoutingKeyCollectionInterface;

class RoutingKeys implements RoutingKeyCollectionInterface
{
    public const ROUTING_KEYS = [
        MembershipCreateCommand::class => 'membership.command.create',
    ];

    public function getRoutingKeys(): array
    {
        return self::ROUTING_KEYS;
    }
}
