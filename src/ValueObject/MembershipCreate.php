<?php

declare(strict_types=1);

namespace BNNVARA\Membership\ValueObject;

class MembershipCreate
{
    private string $membershipId;
    private string $initials;
    private string $firstName;
    private ?string $nameAffix = null;
    private string $lastName;
    private string $gender;
    private string $dateOfBirth;
    private string $postalCode;
    private int $houseNumber;
    private ?string $houseNumberAddition = null;
    private string $street;
    private string $city;
    private string $emailAddress;
    private string $phoneNumber;
    private string $actionCode;
    private ?string $premiumCode = null;
    private int $membershipCost;

    public function __construct(
        string $membershipId,
        string $initials,
        string $firstName,
        string $lastName,
        string $gender,
        string $dateOfBirth,
        string $postalCode,
        int $houseNumber,
        string $street,
        string $city,
        string $emailAddress,
        string $phoneNumber,
        string $actionCode,
        int $membershipCost,
        ?string $nameAffix = null,
        ?string $houseNumberAddition = null,
        ?string $premiumCode = null
    ) {
        $this->membershipId = $membershipId;
        $this->initials = $initials;
        $this->firstName = $firstName;
        $this->nameAffix = $nameAffix;
        $this->lastName = $lastName;
        $this->gender = $gender;
        $this->dateOfBirth = $dateOfBirth;
        $this->postalCode = $postalCode;
        $this->houseNumber = $houseNumber;
        $this->houseNumberAddition = $houseNumberAddition;
        $this->street = $street;
        $this->city = $city;
        $this->emailAddress = $emailAddress;
        $this->phoneNumber = $phoneNumber;
        $this->actionCode = $actionCode;
        $this->premiumCode = $premiumCode;
        $this->membershipCost = $membershipCost;
    }

    public function getMembershipId(): string
    {
        return $this->membershipId;
    }

    public function getInitials(): string
    {
        return $this->initials;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getNameAffix(): ?string
    {
        return $this->nameAffix;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function getGender(): string
    {
        return $this->gender;
    }

    public function getDateOfBirth(): string
    {
        return $this->dateOfBirth;
    }

    public function getPostalCode(): string
    {
        return $this->postalCode;
    }

    public function getHouseNumber(): int
    {
        return $this->houseNumber;
    }

    public function getHouseNumberAddition(): ?string
    {
        return $this->houseNumberAddition;
    }

    public function getStreet(): string
    {
        return $this->street;
    }

    public function getCity(): string
    {
        return $this->city;
    }

    public function getEmailAddress(): string
    {
        return $this->emailAddress;
    }

    public function getPhoneNumber(): string
    {
        return $this->phoneNumber;
    }

    public function getActionCode(): string
    {
        return $this->actionCode;
    }

    public function getPremiumCode(): ?string
    {
        return $this->premiumCode;
    }

    public function getMembershipCost(): int
    {
        return $this->membershipCost;
    }
}
